Param(
    [parameter(Mandatory=$false,
    HelpMessage="Azure region where resources would be provisioned")]
    [string] $location = "West Europe",

    [parameter(Mandatory=$true,
    HelpMessage="Azure region where resources would be provisioned")]
    [string] $clusterResourceGroupName, 

    [parameter(Mandatory=$false,
    HelpMessage="Azure region where resources would be provisioned")]
    [string] $clusterResourceGroupLocation = "West Europe",
    
    [parameter(Mandatory=$false,
    HelpMessage="Storage account name to deploy the artifacts")]
    [string] $resourceGroupStorageAccountName = "dmzlabrepsol",

    [parameter(Mandatory=$false,
    HelpMessage="Resource group to deploy artifacts")]
    [string] $resourceGroupName = "dmzlab2"
)
 
$ErrorActionPreference = "Stop"
 
## Deployment artifacts configuration variables
$artifactsResourceGroupStorageAccountName = $resourceGroupStorageAccountName
$artifactsResourceGroupLocation = $location
$artifactsResourceGroupName = $resourceGroupName
 
# Add subscription name to avoid prompt message for selecting subscription
$subscription = ""
 
if (-not $subscription) {
    try {
        Write-Output "|--> Obtaining Azure subscriptions available"
        $subscription = Get-AzureRmSubscription
    }
    catch {
        Write-Output "You need to login to Azure before continuing. Please, introduce your credentials in the windows that has appeared"
        Login-AzureRmAccount | Out-Null
        $subscription = Get-AzureRmSubscription
    }
 
    Write-Output "|---> Obtained"
    $workingSubscription = ($subscription |  Out-GridView -Title "Select the subscription to deploy" -OutputMode Single)
 
    Select-AzureRmSubscription -SubscriptionName $workingSubscription.Name | Out-Null
}
else {
    Write-Output "|--> Configuring subscription $subscription"
    Select-AzureRmSubscription -SubscriptionName $subscription
    Write-Output "|---> Configured"
}
 
if ( -Not (Get-AzureRmResourceGroup -Name $artifactsResourceGroupName -Location $artifactsResourceGroupLocation -ErrorAction SilentlyContinue) ) {
    Write-Output "|--> Creating $artifactsResourceGroupName artifacts resource group"
    New-AzureRmResourceGroup -Name $artifactsResourceGroupName -Location $artifactsResourceGroupLocation | Out-Null
    Write-Output "|---> Created"
}
else {
    Write-Output "|--> An artifacts resource group already exists. Skipping"
}
 
if ( -Not (Get-AzureRmStorageAccount -ResourceGroupName $artifactsResourceGroupName -ErrorAction SilentlyContinue | Where-Object { $_.StorageAccountName -like "$artifactsResourceGroupStorageAccountName*" } ) ) {
    Write-Output "|--> Creating $artifactsResourceGroupStorageAccountName artifacts storage account"
    New-AzureRmStorageAccount -ResourceGroupName $artifactsResourceGroupName -Location $artifactsResourceGroupLocation `
        -Name $artifactsResourceGroupStorageAccountName -SkuName Standard_GRS -Kind Storage | Out-Null
    Write-Output "|---> Created"
}
else {
    Write-Output "|--> An artifacts storage account already exists. Skipping"
    $artifactsResourceGroupStorageAccountName = $(Get-AzureRmStorageAccount -ResourceGroupName $artifactsResourceGroupName -ErrorAction SilentlyContinue | Where-Object { $_.StorageAccountName -like "$artifactsResourceGroupStorageAccountNameP*" } | Select-Object StorageAccountName).StorageAccountName
}
 
# Obtaining files
$filesToUpload = Get-ChildItem ".\*" -Include *.json, *.zip, *.ps1
$artifactsResourceGroupStorageAccountKey = Get-AzureRmStorageAccountKey -ResourceGroupName $artifactsResourceGroupName -Name $artifactsResourceGroupStorageAccountName
$storageContext = New-AzureStorageContext -StorageAccountName $artifactsResourceGroupStorageAccountName -StorageAccountKey $artifactsResourceGroupStorageAccountKey[0].Value
 
if ( -Not (Get-AzureStorageContainer -Name "artifacts" -Context $storageContext -ErrorAction SilentlyContinue) ) {
    Write-Output "|--> Creating artifacts container"
    New-AzureStorageContainer -Name "artifacts" -Permission Blob -Context $storageContext | Out-Null
    Write-Output "|---> Created"
}
else {
    Write-Output "|--> An artifacts container already exists. Skipping"
}
 
foreach ( $file in $filesToUpload) {
    Write-Output "|--> Uploading file $($file.Name)"
    Set-AzureStorageBlobContent -File $file.Name -Container "artifacts" -Blob $file.Name -Context $storageContext -Force | Out-Null
    Write-Output "|---> Uploaded"
}
 
$artifactsBaseUri = $storageContext.BlobEndPoint + "artifacts"
 
if ( -Not (Get-AzureRmResourceGroup -Name $clusterResourceGroupName -Location $clusterResourceGroupLocation -ErrorAction SilentlyContinue) ) {
    Write-Output "|--> Creating resource group"
    New-AzureRmResourceGroup -Name $clusterResourceGroupName -Location $clusterResourceGroupLocation -Force | Out-Null
    Write-Output "|---> Created"
}
else {
    Write-Output "|---> A resource group already exists. Skipping"
}
 
Write-Output "|--> Starting deployment of the cluster"
New-AzureRmResourceGroupDeployment -Name "deployment-$((Get-Date).Ticks)" -ResourceGroupName $clusterResourceGroupName  `
    -Mode Incremental -TemplateUri "$artifactsBaseUri/template.json" -TemplateParameterUri "$artifactsBaseUri/parameters.json"  `
    -artifactsBaseUri $artifactsBaseUri `
    -Verbose